from fabric import *
import os
#copy file-file python ke opestack server
scp = "scp /media/hdd/TELKOM\ INSTITUTE\ OF\ TECHNOLOGY/RESEARCH/Automation\ Tool\ Python/*.py pemsim@10.0.0.15:/home/pemsim"
os.system(scp)

#ssh ke openstack server
result = Connection(
"pemsim@10.0.0.15:22".format(
    username='pemsim',
    ip ='10.0.0.15',
    port=22,
),
connect_kwargs={"password":'pemodelan123'}
)

#command-command
commands = [
    "sudo yum -y update",
    "sudo systemctl stop firewalld.service && sudo systemctl disable firewalld.service",
    "sudo setenforce 0",
    "sudo sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config",
    "sudo DEBIAN_FRONTEND=noninteractive yum install python3 -y && yum autoremove -y",
    "sudo yum -y update",
    "python3 openstack.py" #Go to openstack.py
]
for command in commands:
    print("\n", "="*10, command, "="*10, "\n")
    result.run(command,pty=True, warn=True)
