import os
commands = [
    "sudo yum -y install vim wget screen crudini htop nano",
    "sudo yum install -y centos-release-openstack-rocky",
    "sudo yum install -y https://rdoproject.org/repos/rdo-release.rpm",
    "sudo rm -rf /etc/yum.repos.d/CentOS-Ceph-Luminous.repo",
    "sudo rm -rf /etc/yum.repos.d/CentOS-OpenStack-rocky.repo",
    "sudo yum -y update'",
    "sudo yum install -y openstack-packstack",
    "sudo packstack --gen-answer-file=15-openstack.txt",
    "python3 answerfile.py",
    "sudo packstack --answer-file=15-openstack.txt"
]
for command in commands:
    print("\n","#"*5, command, "#"*5,"\n")
    os.system(command)