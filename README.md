## Build Openstack Packstack with Python ##

Single Node Openstack :
* OS : Centos 7
* Adapter : Host-only Adapter (vboxnet0) (Masquerade from PC Host)
* IP Address : 10.0.0.10/24
* Gateway : 10.0.0.1
* DNS1 : 10.0.0.10
* DNS2 : 8.8.8.8
* DNS3 : 8.8.4.4



**++++++ Setup Node ++++++**


1.  Configure name resolution in /etc/hosts

>  10.0.0.10 openstack


2.  Disable Network Manager & Enable Network Service


> systemctl disable NetworkManager.service

> systemctl stop NetworkManager.service

> systemctl status NetworkManager.service

> systemctl enable network.service

> systemctl restart network.service

> systemctl status network.service


3.  IP Address Configuration in /etc/sysconfig/network-scripts/ifcfg-enp0s3


4.  Update dan upgrade node

> yum -y update && yum -y upgrade 

5. Create id_rsa.pub root

> sudo ssh-keygen 

6. Reboot node
