import os
files = [
    "sudo sed -i 's/CONFIG_CEILOMETER_INSTALL\=y/CONFIG_CEILOMETER_INSTALL\=n/g' 15-openstack.txt",
    "sudo sed -i 's/CONFIG_AODH_INSTALL\=y/CONFIG_AODH_INSTALL\=n/g' 15-openstack.txt",
    "sudo sed -i 's/CONFIG_KEYSTONE_ADMIN_PW/#CONFIG_KEYSTONE_ADMIN_PW/g' 15-openstack.txt",
    "sudo sed -i '338 i CONFIG_KEYSTONE_ADMIN_PW=adminPass' 15-openstack.txt",
    "sudo sed -i 's/CONFIG_NEUTRON_OVS_BRIDGE_IFACES\=/CONFIG_NEUTRON_OVS_BRIDGE_IFACES\=br-ex:enp0s3/g' 15-openstack.txt",
    "sudo sed -i 's/CONFIG_NEUTRON_OVS_BRIDGES_COMPUTE\=/CONFIG_NEUTRON_OVS_BRIDGES_COMPUTE\=br-ex/g' 15-openstack.txt",
    "sudo sed -i 's/CONFIG_PROVISION_DEMO\=y/CONFIG_PROVISION_DEMO\=n/g' 15-openstack.txt"
]
print("\n","#"*5,"Answer File","#"*5,"\n")
for file in files:
    os.system(file)

print("\n","#"*5,"Answer File DONE","#"*5,"\n")